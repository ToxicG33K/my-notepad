package rs.itcentar.my.notepad;

import java.io.File;
import javax.swing.filechooser.FileFilter;

/**
 *
 * @author IQooLogic
 */
public class TextFileFilter extends FileFilter {

    @Override
    public boolean accept(File f) {
        if (f.isDirectory()) {
           return true;
       } else {
           String filename = f.getName().toLowerCase();
           return filename.endsWith(".txt") || filename.endsWith(".log")
                   || filename.endsWith(".nfo") || filename.endsWith(".xml")
                   || filename.endsWith(".json");
       }
    }

    @Override
    public String getDescription() {
        return "Text File (*.txt)";
    }
}
