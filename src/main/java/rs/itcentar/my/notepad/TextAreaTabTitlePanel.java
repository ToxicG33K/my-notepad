package rs.itcentar.my.notepad;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

/**
 *
 * @author IQooLogic
 */
public class TextAreaTabTitlePanel extends JPanel {

    private JTabbedPane tpOpenedFiles;
    private TextAreaTabPanel panel;
    private String title;

    public TextAreaTabTitlePanel(String title, JTabbedPane tpOpenedFiles, TextAreaTabPanel panel) {
        this.tpOpenedFiles = tpOpenedFiles;
        this.panel = panel;
        this.title = title;
        initComponents();

        lTitle.setText(title);
    }

    public String getTitle() {
        return title;
    }

    public String getChangedTitle() {
        return "<html><b><i>" + getTitle() + "*</i></b></html>";
    }

    public void setTitle(String title) {
        lTitle.setText(title);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lTitle = new javax.swing.JLabel();
        bClose = new javax.swing.JButton();

        setOpaque(false);

        lTitle.setFocusable(false);

        bClose.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        bClose.setText("X");
        bClose.setBorderPainted(false);
        bClose.setContentAreaFilled(false);
        bClose.setFocusable(false);
        bClose.setMargin(new java.awt.Insets(0, 0, 0, 0));
        bClose.setMaximumSize(new java.awt.Dimension(12, 12));
        bClose.setMinimumSize(new java.awt.Dimension(12, 12));
        bClose.setPreferredSize(new java.awt.Dimension(12, 12));
        bClose.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bCloseActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(lTitle, javax.swing.GroupLayout.DEFAULT_SIZE, 92, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bClose, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lTitle, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(bClose, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 20, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void bCloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bCloseActionPerformed
        if (panel.isChanged()) {
            int result = JOptionPane.showConfirmDialog(TextAreaTabTitlePanel.this, "Do you want to save changes ?", "File is not saved", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
            if (result == JOptionPane.YES_OPTION) {
                panel.save();
            }
        }

        tpOpenedFiles.remove(panel);
        if (panel.getFile() != null) {
            HistoryManager.remove(panel.getFilePath());
        }
    }//GEN-LAST:event_bCloseActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bClose;
    private javax.swing.JLabel lTitle;
    // End of variables declaration//GEN-END:variables
}
