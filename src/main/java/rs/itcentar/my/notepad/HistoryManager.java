package rs.itcentar.my.notepad;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author IQooLogic
 */
public class HistoryManager {
    private static final Map<String, TextAreaTabPanel> openedPanels = new HashMap<>();

    public static Map<String, TextAreaTabPanel> getOpenedPanels() {
        return openedPanels;
    }
    
    public static void add(String path, TextAreaTabPanel panel) {
        openedPanels.put(path, panel);
    }
    
    public static void remove(String path) {
        openedPanels.remove(path);
    }
    
    public static TextAreaTabPanel getPanel(String path) {
        return openedPanels.get(path);
    }
}
