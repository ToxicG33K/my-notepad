package rs.itcentar.my.notepad;

import java.awt.Toolkit;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class NotepadFrame extends javax.swing.JFrame {

    private final JFileChooser fc = new JFileChooser();

    public NotepadFrame() {
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/app.png")));
        
        initComponents();

        fc.setFileFilter(new TextFileFilter());

        tpOpenedFiles.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                String filePath = "";
                TextAreaTabPanel panel = (TextAreaTabPanel) tpOpenedFiles.getSelectedComponent();
                if (panel != null && panel.getFile() != null) {
                    filePath = panel.getFilePath();
                }
                lStatusBar.setText(filePath);
            }
        });

        addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {
            }

            @Override
            public void windowClosing(WindowEvent e) {
                saveOnClose();
            }

            @Override
            public void windowClosed(WindowEvent e) {
            }

            @Override
            public void windowIconified(WindowEvent e) {
            }

            @Override
            public void windowDeiconified(WindowEvent e) {
            }

            @Override
            public void windowActivated(WindowEvent e) {
            }

            @Override
            public void windowDeactivated(WindowEvent e) {
            }
        });
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lStatusBar = new javax.swing.JLabel();
        tpOpenedFiles = new javax.swing.JTabbedPane();
        mbNotepad = new javax.swing.JMenuBar();
        mFile = new javax.swing.JMenu();
        miNewFile = new javax.swing.JMenuItem();
        miOpen = new javax.swing.JMenuItem();
        miSave = new javax.swing.JMenuItem();
        separator1 = new javax.swing.JPopupMenu.Separator();
        miClose = new javax.swing.JMenuItem();
        mHelp = new javax.swing.JMenu();
        miAbout = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("MyNotepad v1.0.0");

        lStatusBar.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        mFile.setText("File");

        miNewFile.setText("New");
        miNewFile.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miNewFileActionPerformed(evt);
            }
        });
        mFile.add(miNewFile);

        miOpen.setText("Open");
        miOpen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miOpenActionPerformed(evt);
            }
        });
        mFile.add(miOpen);

        miSave.setText("Save");
        miSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miSaveActionPerformed(evt);
            }
        });
        mFile.add(miSave);
        mFile.add(separator1);

        miClose.setText("Close");
        miClose.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miCloseActionPerformed(evt);
            }
        });
        mFile.add(miClose);

        mbNotepad.add(mFile);

        mHelp.setText("Help");

        miAbout.setText("About");
        mHelp.add(miAbout);

        mbNotepad.add(mHelp);

        setJMenuBar(mbNotepad);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lStatusBar, javax.swing.GroupLayout.DEFAULT_SIZE, 800, Short.MAX_VALUE)
            .addComponent(tpOpenedFiles)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(tpOpenedFiles, javax.swing.GroupLayout.DEFAULT_SIZE, 551, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lStatusBar, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void miOpenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miOpenActionPerformed
        fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
        int ret = fc.showOpenDialog(NotepadFrame.this);
        if (ret == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();
            // open it
            lStatusBar.setText(file.getPath());

            TextAreaTabPanel panel = HistoryManager.getPanel(file.getPath());
            if (panel == null) {
                panel = new TextAreaTabPanel(tpOpenedFiles, file, file.getName());
            }

            panel.open();
            int indexOfTab = tpOpenedFiles.indexOfComponent(panel);
            if (indexOfTab == -1) {
                tpOpenedFiles.addTab(file.getName(), panel);
            }
            int tabIndex = indexOfTab == -1 ? (tpOpenedFiles.getTabCount() - 1) : indexOfTab;
            tpOpenedFiles.setSelectedIndex(tabIndex);
            tpOpenedFiles.setTabComponentAt(tabIndex, panel.getTabTitlePanel());
            HistoryManager.add(file.getPath(), panel);
        }
    }//GEN-LAST:event_miOpenActionPerformed

    private void miSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miSaveActionPerformed
        TextAreaTabPanel panel = (TextAreaTabPanel) tpOpenedFiles.getSelectedComponent();
        if (panel != null) {
            if (panel.getFile() != null) {
                panel.save();
            } else {
                fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
                int ret = fc.showSaveDialog(NotepadFrame.this);
                if (ret == JFileChooser.APPROVE_OPTION) {
                    File file = fc.getSelectedFile();
                    panel.setFile(file);
                    // save it
                    panel.save();
                }
            }
            lStatusBar.setText(panel.getFilePath());
        }
    }//GEN-LAST:event_miSaveActionPerformed

    private void miCloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miCloseActionPerformed
        saveOnClose();

        System.exit(0);
    }//GEN-LAST:event_miCloseActionPerformed

    private void saveOnClose() {
        for (TextAreaTabPanel panel : HistoryManager.getOpenedPanels().values()) {
            if (panel.isChanged()) {
                tpOpenedFiles.setSelectedComponent(panel);
                int result = JOptionPane.showConfirmDialog(NotepadFrame.this,
                        String.format("Do you want to save changes in '%s' ?", panel.getFile().getName()),
                        "File is not saved", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                if (result == JOptionPane.YES_OPTION) {
                    panel.save();
                }
            }
        }
    }

    private void miNewFileActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miNewFileActionPerformed
        TextAreaTabPanel panel = new TextAreaTabPanel(tpOpenedFiles, null, TextAreaTabPanel.DEFAULT_TAB_TITLE);
        tpOpenedFiles.add(panel);
        tpOpenedFiles.setSelectedIndex(tpOpenedFiles.getTabCount() - 1);
        tpOpenedFiles.setTabComponentAt(tpOpenedFiles.indexOfComponent(panel), panel.getTabTitlePanel());
    }//GEN-LAST:event_miNewFileActionPerformed

    public static void main(String args[]) {
        try {
            javax.swing.UIManager.setLookAndFeel(javax.swing.UIManager.getCrossPlatformLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(NotepadFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                NotepadFrame frame = new NotepadFrame();
                frame.setLocationRelativeTo(null);
                frame.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel lStatusBar;
    private javax.swing.JMenu mFile;
    private javax.swing.JMenu mHelp;
    private javax.swing.JMenuBar mbNotepad;
    private javax.swing.JMenuItem miAbout;
    private javax.swing.JMenuItem miClose;
    private javax.swing.JMenuItem miNewFile;
    private javax.swing.JMenuItem miOpen;
    private javax.swing.JMenuItem miSave;
    private javax.swing.JPopupMenu.Separator separator1;
    private javax.swing.JTabbedPane tpOpenedFiles;
    // End of variables declaration//GEN-END:variables
}
