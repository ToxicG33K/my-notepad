package rs.itcentar.my.notepad;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTabbedPane;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

/**
 *
 * @author IQooLogic
 */
public class TextAreaTabPanel extends javax.swing.JPanel {

    public static final String DEFAULT_TAB_TITLE = "New File";
    private File file;
    private TextAreaTabTitlePanel tabTitlePanel;
    private boolean changed = false;
    
    private DocumentListener documentListener;

    public TextAreaTabPanel(JTabbedPane tpOpenedFiles, File file, String tabTitle) {
        this.file = file;
        this.tabTitlePanel = new TextAreaTabTitlePanel(tabTitle, tpOpenedFiles, this);
        initComponents();
        
        documentListener = new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                setChangedTitle();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                setChangedTitle();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                setChangedTitle();
            }
            
            private void setChangedTitle() {
                tabTitlePanel.setTitle(tabTitlePanel.getChangedTitle());
                changed = true;
            }
        };
        
        if(file == null) {
            taText.getDocument().addDocumentListener(documentListener);
        }
    }

    public boolean isChanged() {
        return changed;
    }

    public void setText(String text) {
        taText.setText(text);
        taText.getDocument().addDocumentListener(documentListener);
    }

    public String getText() {
        return taText.getText();
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
        setTitle(file.getName());
    }

    public String getFilePath() {
        return file.getPath();
    }
    
    public void setTitle(String title) {
        tabTitlePanel.setTitle(title);
    }

    public TextAreaTabTitlePanel getTabTitlePanel() {
        return tabTitlePanel;
    }

    public void save() {
        try (FileWriter fw = new FileWriter(file)) {
            fw.write(getText());
//            tabTitlePanel.setTitle(tabTitlePanel.getTitle());
            tabTitlePanel.setTitle(file.getName());
            changed = false;
        } catch (IOException ex) {
            Logger.getLogger(NotepadFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void open() {
        try (FileReader fr = new FileReader(file);
                BufferedReader br = new BufferedReader(fr)) {
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
                sb.append(System.lineSeparator());
            }

            setFile(file);
            setText(sb.toString());
        } catch (FileNotFoundException ex) {
            Logger.getLogger(NotepadFrame.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(NotepadFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        taText = new javax.swing.JTextArea();

        taText.setColumns(20);
        taText.setRows(5);
        jScrollPane1.setViewportView(taText);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 300, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea taText;
    // End of variables declaration//GEN-END:variables
}
